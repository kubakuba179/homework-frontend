import React, { Component } from 'react';
import superagent from 'superagent';

import Table from './Table';
import Form from './Form';
import ErrorMessage from './ErrorMessage';

const USER_NAME = "userName";

class AppContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            idSequenceStart: 5,
            userForm: {},
            userToEditId: undefined,
            errorMessage: undefined
        }
    }
    componentDidMount() {
        this.loadUsersFromDb();
    }

    render() {
        return (
            <div>
                <Table
                    onRowDelete={this.handleRowDelete}
                    tableTitle={"Users"}
                    tableItems={this.state.users}
                    onRowEdit={this.handleRowEdit}
                />
                <Form
                    formState={this.state.userForm}
                    onFieldValueChange={this.handleFieldValueChange}
                    onSubmit={this.state.userToEditId ? this.handleRowEditSave : this.handleRowAdd}
                    formFields={[{ label: this.state.userToEditId ? "Edit user" : "New user", name: USER_NAME }]}
                    onCancel={this.state.userToEditId && this.handleRowEditCancel}
                    submitButtonLabel={this.state.userToEditId ? "Save" : "Add"}
                />
                <ErrorMessage errorMessage={this.state.errorMessage}/>
            </div>
        );
    }

    // actions //

    handleErrorMessage = (message) => {
        this.setState({
            errorMessage: message
        });
    };

    handleRowDelete = (id) => () => {
        superagent
            .delete(`secured/user/${id}`)
            .then((res) => {
                if (res && res.ok) {
                    this.loadUsersFromDb();
                    this.handleErrorMessage();
                }
            }).catch(err => {
                this.handleErrorMessage(err.message);
            });        
    };

    handleRowAdd = (event) => {
        superagent
            .post('/user')
            .set('Content-Type', 'application/json')
            .send({ name: this.state.userForm[USER_NAME] })
            .then((res) => {
                if (res && res.ok) {
                    this.loadUsersFromDb();
                    this.setState({
                        userForm: {}
                    })
                    this.handleErrorMessage();
                }
            }).catch(err => {
                this.handleErrorMessage(err.message);
            });
        event.preventDefault();
    };

    handleFieldValueChange = (name) => (event) => {
        this.setState({
            userForm: { [name]: event.target.value }
        });
    }

    handleRowEdit = (id, userName) => () => {
        this.setState({
            userToEditId: id,
            userForm: { [USER_NAME]: userName }
        })
    };

    handleRowEditCancel = () => {
        this.setState({
            userToEditId: undefined,
            userForm: {}
        })
    };

    handleRowEditSave = (event) => {
        superagent
            .put(`/user/${this.state.userToEditId}`)
            .set('Content-Type', 'application/json')
            .send({ name: this.state.userForm[USER_NAME] })
            .then((res) => {
                if (res && res.ok) {
                    this.loadUsersFromDb();
                    this.setState({
                        userForm: {},
                        userToEditId: undefined
                    });
                    this.handleErrorMessage();
                }
            }).catch(err => {
                this.handleErrorMessage(err.message);
            });        
        event.preventDefault();
    }

    loadUsersFromDb = () => {
        superagent
            .get('/user')
            .then((res) => {
                if (res && res.ok) {
                    this.setState({
                        users: res.body
                    });
                    this.handleErrorMessage();
                }
            }).catch(err => {
                this.handleErrorMessage(err.message)
            });
    }
}

export default AppContainer;