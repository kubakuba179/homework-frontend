import React from 'react';
import PropTypes from 'prop-types';

import TextField from './TextField';

const Form = ({ formFields, onSubmit, onFieldValueChange, formState, onCancel, submitButtonLabel }) => (
    <form onSubmit={onSubmit}>
        {formFields.map((field, index) =>
            (<label key={index}>
                {field.label}:
                        <br/>
                        <TextField name={field.name} value={formState[field.name] || ''} onChange={onFieldValueChange(field.name)} />
            </label>))}
        <input type="submit" value={submitButtonLabel || "Submit"} />
        {onCancel && <button onClick={onCancel} >Cancel</button>}
    </form>
);

Form.propTypes = {
    formFields: PropTypes.arrayOf(PropTypes.object),
    onSubmit: PropTypes.func.isRequired,
    onFieldValueChange: PropTypes.func.isRequired,
    formState: PropTypes.object.isRequired,
    onCancel: PropTypes.func,
    submitButtonLabel: PropTypes.string
};

export default Form;