import React from 'react';
import PropTypes from 'prop-types';

const TextField = ({name, value, onChange}) => (
    <input type="text" name={name} value={value} onChange={onChange}/>
)

TextField.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func,
};

export default TextField;