import React from 'react';
import PropTypes from 'prop-types';

import Header from './components/Header';
import Body from './components/Body';
import Row from './components/Row';
import Column from './components/Column';

const Table = ({ tableItems, tableTitle, onRowDelete, onRowEdit }) => (
    <div>
        <h1 className={'table-title'}>{tableTitle}</h1>
        <table>
            <Body>
                <Header headerNames={["ID", "NAME", ""]} />
                {tableItems.map(item => (
                    <Row key={item.id}>
                        <Column>{item.id}</Column>
                        <Column>{item.name}</Column>
                        <Column>
                            <button onClick={onRowDelete(item.id)}>Delete</button>
                            <button onClick={onRowEdit(item.id, item.name)}>Edit</button>
                        </Column>
                    </Row>))}
            </Body>
        </table>        
    </div>
);

Table.propTypes = {
    tableItems: PropTypes.arrayOf(PropTypes.object),
    tableTitle: PropTypes.string.isRequired,
    onRowDelete: PropTypes.func.isRequired,    
    onRowEdit: PropTypes.func.isRequired
};

export default Table;