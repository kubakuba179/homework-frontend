
import React from 'react';
import PropTypes from 'prop-types';

const Header = ({ headerNames = [] }) =>
    (<tr>{headerNames.map((headerName, index) => <th key={index}>{headerName}</th>)}</tr>);

Header.propTypes = {
    headerNames: PropTypes.arrayOf(PropTypes.string)    
};

export default Header;