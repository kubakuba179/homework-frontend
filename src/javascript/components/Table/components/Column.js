import React from 'react';

const Column = ({children}) => (
    <td>{children}</td>
)

export default Column;