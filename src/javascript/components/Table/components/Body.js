import React from 'react';

const Body = ({children}) => (
    <tbody>
        {children}
    </tbody>
)

export default Body;