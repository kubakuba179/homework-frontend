import React from 'react';

const ErrorMessage = ({errorMessage}) => (
    <p className="error-message">{errorMessage}</p>
)
export default ErrorMessage;